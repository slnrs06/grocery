package com.grocery.demo.controller;

import com.grocery.demo.model.Grocery;
import com.grocery.demo.services.GroceryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class GroceryController {

//    private GroceryRepository groceriesDao;
//
//    public GroceriesController(GroceryRepository groceriesDao) {
//        this.groceriesDao = groceriesDao;
//
//    }


    @Autowired
    private GroceryService groceryService;

    @GetMapping("/")//goes to the index.html
    public String viewHomePage(Model model){
        return findPaginated(1,"firstName","asc", model);
    }



    @GetMapping("/showNewGroceryForm")
    public String showNewGroceryForm(Model model){
        // create model to bind form data

        Grocery grocery = new Grocery();
        model.addAttribute("grocery", grocery);
        return "add_grocery";
    }


    @PostMapping("/saveGrocery")
    public String saveGrocery(@ModelAttribute("grocery")Grocery grocery){
        //save grocery to database
        groceryService.saveGrocery(grocery);
        return "redirect:/";
    }


    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value="id")long id, Model model){
        // get grocery from service
        Grocery grocery = groceryService.getGroceryById(id);

        //set grocery as a model attribute to pre populate the form

        model.addAttribute("grocery", grocery);
        return "update_grocery";

    }


    @GetMapping("/deleteGrocery/{id}")
    public String deleteGrocery(@PathVariable(value="id")long id){
        // call delete grocery method
        this.groceryService.deleteGroceryById(id);
        return"redirect:/";

    }


    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value="pageNo")int pageNo, @RequestParam("sortField")String sortField, @RequestParam("sortDir")String sortDir, Model model){


        int pageSize = 5;//creating int variable of pageSize assigning value of 5



        Page<Grocery> page=groceryService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<Grocery> listGrocery=page.getContent();



        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("sort", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc")?"desc":"asc");
        model.addAttribute("listGrocery", listGrocery);
        return "index";
    }







//
//    @RequestMapping("/groceries")
//    public String viewGroceriesPage(Model model){
//
//        model.addAttribute("groceries", groceryRepository.findAll());
//        return "groceries";
//
//    }


//@Autowired
//    private GroceryService groceryService;
//
//@GetMapping("/groceries")





//    @GetMapping("/groceries")
//    @ResponseBody
//    public List<Grocery> getAllGroceries(){
//        return groceriesDao.findAll();
//    }
//    @GetMapping("/groceries/search/{title}")
//    public String searchAd(@PathVariable String title, Model model) {
//        model.addAttribute("grocery", groceriesDao.findByTitle(title));
//        return "groceries/search";
//    }





}
